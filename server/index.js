const express = require('express');
const path = require('path');
const Avatar = require('avatar-builder');
//const avatar = Avatar.catBuilder(128);
const avatar = Avatar.male8bitBuilder(128);


const app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

// An api endpoint that returns a short list of items
app.get('/api/getList', (req, res) => {
    var list = ["item1", "item2", "item4"];
    res.json(list);
    console.log('Sent list of items');
});

app.get('/api/getAvatar', (req, res) => {
    // console.log("req", req.query)
    avatar.create(req.query.name).then(buffer => {
        res.end(buffer, 'binary');
    });
});

app.use(express.static('images'))


let clients = [];
let foods = [];

for (let i = 0; i < 10; i++) {
    foods.push({
        id: Math.random().toString(36).substring(7),
        left: Math.random() * 1000,
        top: Math.random() * 1000,
    })
}

io.on('connection', function (socket) {
    console.log("new client connected");
    const id = Math.random().toString(36).substring(7);

    const x = Math.random() * 1000.0;
    const y = Math.random() * 1000.0;

    clients.push({
        id,
        x,
        y,
        speed: 20,
        dirX: 1.0,
        dirY: 0.0,
        tail: [{ x, y }],
        length: 100.0,
        //angle: Math.random() * 360,

    })

    io.emit('clients', { clients });
    io.emit('foods', { foods });

    socket.emit("clientId", id);

    socket.on("disconnect", () => {
        console.log("disconnect", id)
        clients = clients.filter(c => c.id != id);
        socket.emit('clieantLeftTheGame', id);
    });

    socket.on("foodFound", ({ foodId }) => {
        console.log("foodFound", foodId)
        foods = foods.filter(c => c.id != foodId);
        const index = clients.findIndex(c => c.id == id);
        clients[index].length += 100.0;
        socket.emit('lengthChanged', clients[index].length);
        io.emit('removeFood', foodId);
    });

    socket.on("clientDirection", ({ id, top, left, dirX, dirY }) => {
        console.log("clientDirection id", id)
        const index = clients.findIndex(c => c.id == id);

        console.log("index", index, clients)

        clients[index]["tail"].unshift({ x: left, y: top });
        clients[index]["dirX"] = dirX;
        clients[index]["dirY"] = dirY;

        io.emit('clientPositionChanged', { id, top, left, dirX, dirY });
    });



    // socket.on("clientPosition", ({ id, top, left }) => {
    //     console.log("clientPosition", id, top, left)
    //     const index = clients.findIndex(c => c.id != id);

    //     //     const dTop = top - clients[index].top;
    //     //     const dLeft = left - clients[index].left;

    //     //     const angle = (Math.atan2(dTop, dLeft) * 180 / Math.PI) - 90;
    //     //     console.log("angle", dTop, dLeft, angle)

    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    //     // })
    //     //     if (index >= 0) {
    //     //         clients[index].top = top;
    //     //         clients[index].left = left;
    //     //         clients[index].angle = angle;

    //     //         io.emit('clientPositionChanged', { id, top, left, angle });
    //     //     }
    // })
});

const port = process.env.PORT || 5000;
server.listen(port, '0.0.0.0');


console.log('App is listening on port ' + port);





