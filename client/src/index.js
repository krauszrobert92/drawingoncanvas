import React from 'react';
import { render } from 'react-dom';

import './index.css';
import { SnakeGame } from './Game';

render((<SnakeGame />), document.getElementById('root'));