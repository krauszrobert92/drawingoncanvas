import React, { Component, createRef } from "react";
import { fabric } from 'fabric'
import socketIOClient from "socket.io-client"

export class SnakeGame extends Component {


    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);

        this.canvas = new fabric.Canvas("canvas", { backgroundColor: "#0ff" });

        this.canvas.setHeight(window.innerHeight);
        this.canvas.setWidth(window.innerWidth);
        this.canvas.on("mouse:down", this.handleMouseDown);

        this.socket = socketIOClient('http://178.164.201.222:5000/');
        this.socket.on('connect', this.handleClientIsConnected);
        this.socket.on('newClientPosition', this.handleNewClientPosition);
        this.socket.on('clients', this.handleClients);
        this.socket.on('clieantLeftTheGame', this.handleClientLeftTheGame);
        this.socket.on('clientId', this.handleClientId);
        this.socket.on('clientPositionChanged', this.handleClientPositionChanged);
        this.socket.on('foods', this.handleFoods);
        this.socket.on('removeFood', this.handleRemoveFood);
        this.socket.on('lengthChanged', this.handleLengthChanged);


        this.prevTime = new Date();
        this.updateTimer = setInterval(() => {
            const currentTime = new Date();
            const dt = currentTime - this.prevTime;
            this.updateScreen(dt / 1000.0);
            this.prevTime = currentTime;
        }, 1000 / 60);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleClientIsConnected = (data) => {
        console.log("handleClientIsConnected", data);
    }

    handleClientId = (clientId) => {
        console.log("handleClientId", clientId);
        this.clientId = clientId;
    }

    handleFoods = ({ foods }) => {
        initFoods(foods, this.canvas)
    }

    handleRemoveFood = foodId => {
        console.log("handleRemoveFood", foodId)
        this.canvas.getObjects().forEach(o => {
            if (o["type"] === "food" && o["_id"] === foodId)
                this.canvas.remove(o);
        })
    }

    handleClientLeftTheGame = (clientId) => {
        this.canvas.getObjects().forEach(o => {
            if (o["_id"] === clientId)
                this.canvas.remove(o);
        })
    }

    handleLengthChanged = (length) => {
        const object = this.canvas.getObjects().find(o => o["_id"] == this.clientId);

        object["length"] = length;
    }

    handleClientPositionChanged = ({ id, top, left, dirX, dirY }) => {

        const object = this.canvas.getObjects().find(o => o["_id"] == id);

        object["tail"].unshift({ x: left, y: top });
        object["dirX"] = dirX;
        object["dirY"] = dirY;


        const dTop = dirY;
        const dLeft = dirX;

        const angle = (Math.atan2(dTop, dLeft) * 180 / Math.PI) - 90;
        object.angle = angle;

        // object.animate({ left, top }, {
        //     duration: 1000,
        //     onChange: (left, top, test, test2) => {
        //         console.log("options", left, top, test, test2);
        //         for (const food of this.canvas.getObjects()) {
        //             if (food["type"] !== "food" || !food.intersectsWithObject(object)) continue;
        //             console.log("foodFound");
        //             this.socket.emit("foodFound", { id: food["_id"] });
        //             food.opacity = 0.2;
        //         }

        //         this.canvas.renderAll.bind(this.canvas)();
        //     },
        //     easing: fabric.util.ease.easeInQuad,

        // });
    }

    handleClients = (data) => {
        console.log("handleClients", data);
        setClients(data.clients, this.canvas);
    }

    handleNewClientPosition = (data) => {
        console.log("handleNewClientPosition", data);

    }

    handleWindowSizeChange = (event) => {
        this.canvas.setHeight(window.innerHeight);
        this.canvas.setWidth(window.innerWidth);
    }

    handleMouseDown = (options) => {
        const object = this.canvas.getObjects().find(o => o["_id"] == this.clientId);
        const clickX = options.pointer.x;
        const clickY = options.pointer.y;
        const headX = object.left;
        const headY = object.top;
        let dirX = clickX - headX;
        let dirY = clickY - headY;
        const length = Math.sqrt(dirX * dirX + dirY * dirY);
        dirX /= length;
        dirY /= length;
        this.socket.emit("clientDirection", { id: this.clientId, top: object.top, left: object.left, dirX, dirY });
    }

    onChange = (options) => {
        console.log("options", options)
        options.target.setCoords();
    }

    render() {
        return <canvas id="canvas" />
    }

    updateScreen(dt) {
        const object = this.canvas.getObjects().find(o => o["_id"] == this.clientId);
        this.canvas.getObjects().forEach(o => {
            if (o["type"] === "snake_head") {
                updateSnake(o, dt, this.canvas);
            }
        });

        checkFood(object, this.canvas, this.socket);

        this.canvas.renderAll.bind(this.canvas)();
    }
}

function setClients(clients, canvas) {
    canvas.remove(...canvas.getObjects());
    clients.forEach(client => {
        const name = client.id; //Math.random().toString(36).substring(7);
        const radius = 80;

        fabric.Image.fromURL("http://178.164.201.222:5000/api/getAvatar?name=" + name, (img) => {
            img.scale(0.5).set({
                left: client.x,
                top: client.y,
                angle: client.angle,
                selectable: false,
                originX: 'center',
                originY: 'center',
                clipPath: new fabric.Circle({
                    radius: radius,
                    originX: 'center',
                    originY: 'center',
                }),
            });
            img["_id"] = client.id;
            img["type"] = "snake_head";
            img["dirX"] = client.dirX;
            img["dirY"] = client.dirY;
            img["speed"] = client.speed;
            img["tail"] = client.tail;
            img["length"] = client.length;
            canvas.add(img);
        });
    })
}

function initFoods(foods, canvas) {
    foods.forEach(food => {
        const name = food.id; //Math.random().toString(36).substring(7);

        fabric.Image.fromURL("http://178.164.201.222:5000/food2.png", (img) => {
            img.scale(0.03).set({
                left: food.left,
                top: food.top,
                angle: 0,
                selectable: false,
                originX: 'center',
                originY: 'center',

            });
            img["_id"] = food.id;
            img["type"] = "food";
            canvas.add(img);
            canvas.bringForward(img);
        });
    })
}


function updateSnake(snakeHead, dt, canvas) {


    const speed = snakeHead["speed"];
    const vx = snakeHead["dirX"] * speed;
    const vy = snakeHead["dirY"] * speed;

    const dx = vx * dt;
    const dy = vy * dt;

    snakeHead.left += dx;
    snakeHead.top += dy;

    const snakeLength = snakeHead["length"];
    const tail = snakeHead["tail"];

    let currentLength = 0;

    //console.log("tail", tail)


    for (let index in tail) {
        const prevPos = Number(index) == 0 ? ({ x: snakeHead.left, y: snakeHead.top }) : tail[Number(index) - 1];
        const distance = getDistance(prevPos.x, prevPos.y, tail[index].x, tail[index].y);

        currentLength += distance;

        //console.log("distance", distance)


        if (currentLength > snakeLength) {
            const desiredSectionLength = distance - (currentLength - snakeLength);

            const sectionFactor = desiredSectionLength / distance;

            // console.log("desiredSectionLength", desiredSectionLength, "distance", distance, "sectionFactor", sectionFactor, tail[index])


            tail[index].x = prevPos.x + (tail[index].x - prevPos.x) * sectionFactor
            tail[index].y = prevPos.y + (tail[index].y - prevPos.y) * sectionFactor

            for (let i = Number(index) + 1; i < tail.length; i++) {
                if (tail[i].line) {
                    canvas.remove(tail[i].line);
                    tail[i].line = null;
                }

            }

            snakeHead["tail"] = tail.slice(0, index + 1);
            updateTailLine(snakeHead["tail"][index], prevPos, canvas);
            break;
        }
        updateTailLine(tail[index], prevPos, canvas);
    }

}

function getDistance(x1, y1, x2, y2) {
    const x = x2 - x1;
    const y = y2 - y1;
    return Math.sqrt(x * x + y * y);
}

function updateTailLine(tail, prevTail, canvas) {
    //console.log("updateTailLine", tail, prevTail)
    if (!tail.line) {
        tail.line = new fabric.Line([tail.x, tail.y, prevTail.x, prevTail.y], {
            fill: 'green',
            stroke: 'green',
            // backgroundColor: "blue",
            //strokeDashArray: [5, 5],
            borderColor: "black",
            strokeWidth: 10,
            selectable: false,
            evented: false,
        });

        canvas.add(tail.line)
        canvas.sendBackwards(tail.line);

        return;
    } else {
        tail.line.set({
            "x1": tail.x,
            "y1": tail.y,
            "x2": prevTail.x,
            "y2": prevTail.y,
        })

    }
    canvas.sendBackwards(tail.line);

}

function checkFood(snakeHead, canvas, socket) {
    if (!snakeHead)
        return;
    const foods = canvas.getObjects().filter(f => f.type === "food");
    //console.log("foods", foods);
    for (const food of foods) {

        const distance = getDistance(snakeHead.left, snakeHead.top, food.left, food.top);
        //console.log("distance", distance);

        if (distance < 20) {
            console.log("foodFound");
            socket.emit("foodFound", { id: food["_id"] });
        }

        //console.log(food.top, food.left);

        //     if (food["type"] !== "food" || !food.intersectsWithObject(snakeHead)) continue;
        //     console.log("foodFound");
        //     this.socket.emit("foodFound", { id: food["_id"] });
    }
}
// object.animate({ left, top }, {
        //     duration: 1000,
        //     onChange: (left, top, test, test2) => {
        //         console.log("options", left, top, test, test2);
        //         for (const food of this.canvas.getObjects()) {
        //             if (food["type"] !== "food" || !food.intersectsWithObject(object)) continue;
        //             console.log("foodFound");
        //             this.socket.emit("foodFound", { id: food["_id"] });
        //             food.opacity = 0.2;
        //         }

        //         this.canvas.renderAll.bind(this.canvas)();
        //     },
        //     easing: fabric.util.ease.easeInQuad,

        // });